import Vue from 'vue'
import Vuex from 'vuex'
import Axios from "axios"


Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        obj: {},
        bodyShow: true,
        msg: "Введите Слаг"
    },
    getters: {
      INFO: state => {
        return state.obj;
      },
      ERROR: state => {
        return state.msg
      },
      SHOWBODY: state => {

        return state.bodyShow
      }

    },
    mutations: {
      SET_INFO: (state, payload) => {
        state.obj = payload.data;
        state.bodyShow = false
      },
      SET_ERROR: (state, error) => {
        state.msg = error
        state.bodyShow = true
      }
    },
    actions: {
        GET_INFO: async (context, val) => {
          Axios.get('https://tabler.ru/api/v1/places/' + val)
            .then(response => {
              console.log(1223);
              context.commit('SET_INFO', response)
            })
            .catch(function (error) {
              if (error.response.status == 404) {
                context.commit('SET_ERROR', error.response.data.message + " Попробуйте снова")
              }
            })
        }
    }
  })
