import Vue from 'vue'
import App from './App.vue'
import store from './store'
import BootstrapVue from 'bootstrap-vue'
import './scss/required.scss'
Vue.use(BootstrapVue)

new Vue({
  store,
  el: '#app',
  render: h => h(App)
})
